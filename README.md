# Kaggle - House Prices: Advanced Regression Techniques

With 79 explanatory variables describing almost every aspect of residential homes in Ames, Iowa, this 
competition challenges the data science community to predict the final price of each home.


### Data

[house-prices-advanced-regression-techniques download data](https://www.kaggle.com/c/house-prices-advanced-regression-techniques/data)

train.csv: 1460 houses with 81 attributes, including the labels (sale prices)<br>
test.csv: 1459 houses with 80 attributes<br>
data_description.txt: full description of each column of the csv files

### Results
Feature engineering and a solution using lasso and Ridge Regression are shown in [Hauspricekaggel.ipynb](Hauspricekaggel.ipynb).

#### Project steps


 
1. **Data Exploration**  :  This is where I understand patterns and biases in my data. By analyzing a random subset of data using Pandas,
                     I used the graph, graph curve and distribution to see the general trend, 
                     and I created an interactive visualization that allows me to dive into every data point and explore the story behind outliers.


2. **Feature Engineering**:  in this step I have separated some features to make them more informative.



3. **Data Modeling**  :  I started with linear regression and i tried to improve the model with lasso and Ridge regression.


3. **Evaluated data model** :  with cross_val_score , GridSearchCV.  


### Required libraries
- ``NumPy``
- ``Pandas``
- ``scikit-learn``
- ``seaborn``
- ``matplotlib``

### Reference
[House Prices: Advanced Regression Techniques](https://www.kaggle.com/c/house-prices-advanced-regression-techniques)
